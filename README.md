# PandaCapacityAR

> This project is a more up-to-date version of the project: [hololens_polytopes](https://gitlab.inria.fr/auctus-team/people/clairehouziel/public/projets_unity/hololens_polytopes)

Unity HoloLens project to visualize Panda capacity calculations in Augmented Reality (AR). Complements the [pycapacity](https://github.com/auctus-team/pycapacity) package and interfaces with ROS architectures using the [Unity Robotics Hub](https://github.com/Unity-Technologies/Unity-Robotics-Hub).

More specifically, this is a Unity applicaiton for HoloLens 2 that visualises the polytopes and the Panda robot by listening to the `panda/polytope` and `panda/joint_states` ROS message. Additionally the Unity app allows for spawning the robot using a QR code placed in the environment. 

## Prerequisites

 * [HoloLens 2 dependencies](https://learn.microsoft.com/en-us/windows/mixed-reality/develop/install-the-tools)
 * [Unity 2021.3.18f1 LTS](https://learn.unity.com/tutorial/install-the-unity-hub-and-editor#)
 * [MRTK2](https://learn.microsoft.com/en-us/windows/mixed-reality/mrtk-unity/mrtk2/?view=mrtkunity-2022-05)


## Setup & Installation

Open the project in Unity and follow these steps:
1) If you have troubles opening the project on a first setup, with a "compilation errors" message and the option between entering in "Safe Mode" or "Ignore", then choose **Ignore**.
2) An MRTK window _may_ appear on your first setup. Simply skip through the steps, clicking only "Import TMP Essentials" when prompted.
3) Ensure that the "MainScene" is active. Open the "MainScene" in the Unity Editor by selecting it from "File->Open Scene".
4) **Important:** Errors will show up when you first configure this project. This is fine, but in the Unity Editor you will need to configure the project to target UWP ARM64. Go to "File->Build Settings", set the Platform as "UWP" and change the architecture type to "ARM64".
5) The project should currently be configured for deployment alongside ROS Noetic packages, with a connection bridge enabled by the [ROS-TCP-Connector](https://github.com/Unity-Technologies/ROS-TCP-Connector). Please note that if using ROS alongside this HoloLens app, then you will need to enable "Connect on Startup" under the Editor's "Robotics->ROS Settings" panel AND enter your ROS machine's IP address in the "ROS IP Address" field. You should also follow [these instructions](https://github.com/Unity-Technologies/Unity-Robotics-Hub/blob/main/tutorials/ros_unity_integration/setup.md#-ros-environment) on how to configure your ROS workspace.

# Deploy the Unity app to HoloLens 2

In addition to these instructions, general instructions on deploying apps from Visual Studio to the HoloLens can be found [here](https://learn.microsoft.com/en-us/windows/mixed-reality/develop/advanced-concepts/using-visual-studio?tabs=hl2).

<details><summary>Step by step guide</summary>

1) Open build settings

<img src="images/step1.jpg" width="1000px">

2) Select `Universal Windows Platform` 
    - If you do not have this option, you will have to install it
    - Make sure that the Architecture is `AMD64`
    - Click on `Build`

<img src="images/step2.jpg" width="1000px">

3) Select the folder to build it in (for example `build`)

<img src="images/step3.jpg" width="1000px">

4) Open the built solution (for example in the `build` folder)
    - Double click on `PandaCapacityAR.sln`
    - This will open the Visual Studio 2019

<img src="images/step4.jpg" width="1000px">

5) In Visual Studio
    - Make sure to set the achitecture to `AMD64`
    - And to `Release`

<img src="images/step5.jpg" width="1000px">

6) Open Debbuging properties 

<img src="images/step6.jpg" width="1000px">

7) Set the IP address of the HoloLens to the computer name

<img src="images/step7.jpg" width="1000px">

8) Now you can deploy the solution

<img src="images/step8.jpg" width="1000px">

</details>


# QR codes

- You can either use the QR code next to the robot to spawn the AR robot on top of the real robot or you can use the plastified QR codes to spawn the AR anywhere in the room. 

<img src="images/qr_codes.jpg" height="250px">

<details><summary>Original QR code</summary>

- Here is the QR code if you wanna print it yourself.

<img src="images/qrcode.png" height="250px">

</details>


<details><summary>Change spawning position</summary>

If you wanna change the position of the robot with respect to the QR code change the position in the [MoveRobot.cs file](Assets\Projet\Scripts\MoveRobot.cs)
 - robot's position: line 10
 - robot's orientation: line 13

</details>



# ROS side

This application, once deployed, should be used in tandem with the ros catkin workspace from repo [polytopes_ros_ar](https://gitlab.inria.fr/auctus-team/people/antunskuric/demos/polytopes_ros_ar).

See more information about the ROS side in the README of the [polytopes_ros_ar](https://gitlab.inria.fr/auctus-team/people/antunskuric/demos/polytopes_ros_ar) repository.

The final behavior should be something like this:

<a href="https://gitlab.inria.fr/auctus-team/people/antunskuric/demos/polytopes_ros_ar/-/blob/master/src/panda_capacity/images/hololens.mp4">
<img class="gfm js-lazy-loaded" decoding="async" width="500px" src="https://gitlab.inria.fr/auctus-team/people/antunskuric/demos/polytopes_ros_ar/-/raw/master/src/panda_capacity/images/holo_setup.png" loading="lazy" data-qa_selector="js_lazy_loaded_content">
</a>