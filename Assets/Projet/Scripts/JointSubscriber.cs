using Unity.Robotics.ROSTCPConnector;
using RosMessageTypes.Sensor;
using UnityEngine;
using System.Collections;

public class JointSubscriber : MonoBehaviour
{
    [SerializeField]
    string topicName = "/joint_states";
    [SerializeField]
    GameObject robot;

    // ROS Connector
    ROSConnection mRos;

    private ArticulationBody[] articulationChain;

    private JointStateMsg latestJointState; // Variable pour stocker les derni�res donn�es de positions de joints

    const int activeDof = 7;

    void Start()
    {
        // Get ROS connection static instance
        mRos = ROSConnection.GetOrCreateInstance();
        articulationChain = robot.GetComponentsInChildren<ArticulationBody>();
        mRos.Subscribe<JointStateMsg>(topicName, ReceiveJoints);
    }

    private void ReceiveJoints(JointStateMsg jntsMsg)
    {
        StartCoroutine(SetJointValues(jntsMsg));
        latestJointState = jntsMsg; // Stocker les derni�res donn�es de positions de joints re�ues
    }

    IEnumerator SetJointValues(JointStateMsg jntsMsg)
    {

        for (int i = 0; i < activeDof; i++)
        {
            var joint = articulationChain[i + 1];
            var jointDrive = joint.xDrive;
            jointDrive.target = (float)(jntsMsg.position[i]) * Mathf.Rad2Deg;
            joint.xDrive = jointDrive;
        }

        yield return new WaitForSeconds(0.005f);
    }

    // M�thode publique pour acc�der aux derni�res donn�es de positions de joints mises � jour
    public JointStateMsg GetLatestJointState()
    {
        return latestJointState;
    }
}